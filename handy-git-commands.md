`git fetch origin`: get latest stuff from origin

`git remote show origin`: show branches (remote and local, tracked, etc.)

`git remote prune origin`: delete stale branches

`git config core.fscache true`: optimise file storage on Windows
