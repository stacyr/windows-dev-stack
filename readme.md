# My Windows Dev stack

This repository contains installation instructions and configuration/settings for my Windows-based development environment (some information may also be relevant to other operating systems).

The order of sections below is driven by the way which components build upon each other or integrate, it is the order in which I would recommend installing them.

## 1. Package management: Scoop

### Installation

See [scoop.sh](http://scoop.sh/) for instructions - it is as simple as running a single command in PowerShell

### Handy hints

* Install package, e.g. *Node.js Long Term Support (LTS)* version: `scoop install nodejs-lts`
* Get Scoop to update its information about which versions of packages are available: `scoop update`
* See the status of Scoop (i.e. if anything can be updated): `scoop status`
* Upgrade a package to a newer version, i.e. *Node.js LTS*: `scoop update nodejs-lts`
* See the [help docs](https://github.com/lukesampson/scoop/wiki) for more information
* [Buckets](https://github.com/lukesampson/scoop/wiki/Buckets) are collections of packages for Scoop. The default one is called the **master** bucket:

    * Enable the "extras" bucket: `scoop bucket add extras`
    * Package listings for each bucket:

        * [default (master) bucket](https://github.com/ScoopInstaller/Main/tree/master/bucket)
        * [extras bucket](https://github.com/lukesampson/scoop-extras)


## 2. Version control: Git

### Installation via Scoop

`scoop install git-with-openssh`

Make sure to configure your username and email before using (if you haven't already) by running:

* `git config --global user.name "User Name"`
* `git config --global user.email "user@email.com"`

Configure your default pull strategy, i.e. `git config --global pull.rebase false`

### Handy hints

#### .gitignore

A `.gitignore` file tells Git which files to ignore (not track in version control). The ignore file in this repository includes some common defaults - you can also find recommended ignore files online or through your version control provider (GitHub, Bitbucket, etc.). Place `.gitignore` in the root directory of your repository **before** initialising Git.


## 3. Terminal: Windows Terminal + Bash

Bash can be used as an alternative command processor for Windows, it comes with Git so you don't need to install anything else! Windows Terminal is a console emulator application (it runs your selected command processor within a nice window and makes it easy to switch between different command processors).

### Installation via Scoop

`scoop install windows-terminal`  (requires **extras** bucket, see above)

### Handy hints

#### Windows Terminal settings

Enable copying by selecting text in the terminal: go to Terminal settings, under Interaction enable "Automatically copy selection to clipboard"

To use Bash from windows terminal open the settings JSON and paste in the following to the top of the **profiles** **list** array, i.e.:
```
    "profiles": 
    {
        "defaults": {},
        "list": 
        [
          {
            "colorScheme": "Monokai",
            "commandline": "\"%USERPROFILE%\\scoop\\apps\\git-with-openssh\\current\\bin\\bash.exe\" -i -l",
            "font": {
              "face": "Consolas",
              "size": 10.0
            },
            "guid": "{00000000-0000-0000-ba54-000000000002}",
            "historySize": 9001,
            "icon": "%USERPROFILE%\\scoop\\apps\\git-with-openssh\\current\\mingw64\\share\\git\\git-for-windows.ico",
            "name": "Bash",
            "opacity": 80,
            "padding": "0, 0, 0, 0",
            "startingDirectory": "C:\\repos",
            "useAcrylic": true
          },
          ...
            
        ]
```

Then edit the **defaultProfile** value to match the GUID of the new profile above, i.e. `"defaultProfile": "{00000000-0000-0000-ba54-000000000002}",`

Add the _Monokai_ theme to the **schemes** array:
```
    "schemes": 
    [
        {
            "background": "#252729",
            "black": "#000000",
            "blue": "#66D9EF",
            "brightBlack": "#686868",
            "brightBlue": "#66D9EF",
            "brightCyan": "#38CCD1",
            "brightGreen": "#A6E22E",
            "brightPurple": "#AE81FF",
            "brightRed": "#F92672",
            "brightWhite": "#FFFFFF",
            "brightYellow": "#FD971F",
            "cursorColor": "#FFFFFF",
            "cyan": "#38CCD1",
            "foreground": "#F8F8F0",
            "green": "#A6E22E",
            "name": "Monokai",
            "purple": "#AE81FF",
            "red": "#F92672",
            "selectionBackground": "#FFFFFF",
            "white": "#C7C7C7",
            "yellow": "#FD971F"
        },
        
        ...
        
    ]
```

## 4. Editor: Visual Studio Code

### Installation

`scoop install vscode` (requires **extras** bucket, see above)

### Extensions

Some extensions that I find useful:

* *Code Spell Checker*: spellchecker
* *ESLint*: JavaScript linting (see below)
* *Prettier - Code formatter*: formats your code to conform with ESLint syntax (see below)
* *TODO Highlight*: highlights defined flags in code, such as TODO or FIXME

Use the VS Code extensions pane to install (bottom left icon in the editor window)

### Settings

I have included a selection of settings to configure VS Code a bit more to my liking and link in with other parts of this guide (i.e. integrate Bash as the default terminal):

1. Open `vscode-settings.json` from this repository
1. In VS Code *File > Preferences > Settings*
1. Click the `{}` icon in the top right hand corner of the screen to open the `settings.json` file
1. Copy/paste selected settings from `vscode-settings.json`

### Keyboard shortcuts

Some handy keyboard shortcuts (with inspiration from Sublime Text):

* `Ctrl+Shift+up/down`: move current line/block up or down
* `Ctrl+Shift+l`: put a multi-cursor cursor on every line in the selected block
* `Shift+Alt+F`: format the file/selection (this re-indents and applies styles throughout - very handy!)
* `Ctrl+0`: focus the terminal (`Ctrl+1` focuses the active tab in the first editor group - so these two shortcuts can be used to switch back and forth between the terminal and editor)

To install:

1. Open `vscode-keybindings.json` from this repository
1. *File > Preferences > Keyboard Shortcuts*
1. Click: *For advanced customizations open and edit **keybindings.json***
1. Copy/paste from `vscode-keybindings.json` and overwrite `keybindings.json`

### Handy hints

#### Open Folder shortcuts in Windows Explorer right-click menu

1. Navigate to `C:\Users\USERNAME\scoop\apps\vscode\current` (where username is your Windows user name)
1. Double click `vscode-install-context.reg` and accept the warning

## 5. Linters

### Python: .flake8

#### Requirements

* Python is installed and [on your system path](https://pythongisandstuff.wordpress.com/2013/07/10/locating-python-adding-to-path-and-accessing-arcpy/)

#### Installation

1. Install `flake8` PIP package to Python: `python -m pip install flake8`
2. Add the following keys to your VS Code settings:

  * `"python.linting.pylintEnabled": false`
  * `"python.linting.flake8Enabled": true`

#### Project linting

This repository contains a `.flake8` file which you can simply place in your project directory to get started. The `.flake8` file can be edited to match your preferred style, or you can set project-specific styles.

### JavaScript: ESLint

#### Requirements

* Node.js

#### Installation

1. Install `eslint` in your code repositories as a dev dependency (not globally)

#### Project linting

This repository contains a `.eslintrc` file which you can simply place in your project directory to get started. You can also place it in subfolders for specific overrides that will apply in that subfolder and its subfolders. The `.eslintrc` file can be edited to match your preferred style, or you can set project-specific styles.

#### Automatic formatting

In VS Code **Shift + Alt + F** will format your code according to some rules - using the *Prettier - Code formatter* extension. You can get this formatter to use the ESLint styles you already have. Simply install the extension and add the following key to your VS Code settings: `"prettier.eslintIntegration": true`
