
## Create a Junction folder on Windows

This is a kind of Symbolic link that can be used to point a folder to a different location

`mklink /J <Link> <Target>`

e.g.: `mklink /J d_projects D:\projects`


## Configure SSH keys for multiple Bitbucket accounts

If you have multiple Bitbucket accounts but want to access them via SSH from the same PC it requires a bit more set up:

[https://stackoverflow.com/questions/21139926/how-to-maintain-multiple-bitbucket-accounts-with-multiple-ssh-keys-in-the-same-s](https://stackoverflow.com/questions/21139926/how-to-maintain-multiple-bitbucket-accounts-with-multiple-ssh-keys-in-the-same-s)
